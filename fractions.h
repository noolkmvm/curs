#ifndef FRACTIONS_H
#define FRACTIONS_H

#include <iostream>

using namespace std;

class fractions {
public:
    fractions() : verh(0), niz(1) {};

    fractions(int a, int b) {
        if(b) {
            verh = a;
            niz = b;
            toShorten();
            setSign();
        } else {
            verh = 0;
            niz = 1;
        }
    };

    fractions(double num, int pres = 1000000) {
        if (pres <= 0) {
            verh = 0;
            niz = 1;
        } else {
            verh = (int) (num * pres);
            niz = pres;
            toShorten();
            setSign();
        }
    };

    fractions operator+ (const fractions& sec) {
        int resDenom = NOK(this->niz, sec.niz);
        int resNumer = this->verh * (resDenom / this->niz) + sec.verh * (resDenom / sec.niz);
        fractions res(resNumer, resDenom);
        return res;
    };

    fractions operator+ (const double& sec) {
        fractions tmpSec(sec);
        fractions res = *this + tmpSec;
        return res;
    };

    fractions operator- (const fractions& sec) {
        int resDenom = NOK(this->niz, sec.niz);
        int resNumer = this->verh * (resDenom / this->niz) - sec.verh * (resDenom / sec.niz);
        fractions res(resNumer, resDenom);
        return res;
    };

    fractions operator- (const double& sec) {
        fractions tmpSec(sec);
        fractions res = *this - tmpSec;
        return res;
    };

    fractions operator* (const fractions& sec) {
        int resDenom = this->niz * sec.niz;
        int resNumer = this->verh * sec.verh;
        fractions res(resNumer, resDenom);
        return res;
    };

    fractions operator* (const double& sec) {
        fractions tmpSec(sec);
        fractions res = *this * tmpSec;
        return res;
    };

    fractions operator/ (const fractions& sec) {
        int resDenom = this->niz * sec.verh;
        int resNumer = this->verh * sec.niz;
        fractions res(resNumer, resDenom);
        return res;
    };

    fractions operator/ (const double& sec) {
        fractions tmpSec(sec);
        fractions res = *this / tmpSec;
        return res;
    };

    bool operator> (const fractions& sec) {
        int resNumer = this->verh - sec.verh;
        return resNumer > 0;
    };

    bool operator> (const double& sec) {
        fractions tmpSec(sec);
        return *this > tmpSec;
    };

    bool operator< (const fractions& sec) {
        int resNumer = this->verh - sec.verh;
        return resNumer < 0;
    };

    bool operator< (const double& sec) {
        fractions tmpSec(sec);
        return *this < tmpSec;
    };

    bool operator== (const fractions& sec) {
        int resNumer = this->verh - sec.verh;
        return resNumer == 0;
    };

    bool operator== (const double& sec) {
        fractions tmpSec(sec);
        return *this == tmpSec;
    };

    void print() {
        if (verh == 0) cout << "0";
        else if (niz == 1) cout << verh;
        else if (verh != 0) cout << verh << "/" << niz;
    }

private:
    int verh;
    int niz;

    int GCD(int a, int b) {
        return b ? GCD(b, a % b) : a;
    };

    int NOK(int a, int b) {
        return a * b / GCD(a, b);
    };

    void toShorten() {
        int k = GCD(verh, niz);
        verh /= k;
        niz /= k;
    };
    void setSign() {
        if (niz < 0)
        {
            verh *= -1;
            niz *= -1;
        }
    }
};

#endif
