#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include "fractions.h"
#include <unistd.h>

int i, j, c, k, p_max, i_max, baz[100], peres[100], map[100], flag = 0, flag_out = 0, n = 3, m = 2, znak[100];
fractions per, transp, znach, znach_dv, otv, minimum, min_one = -1;
fractions a[100][100], a_const[100][100], a_const1[100][100], b[100], b_const[100], p[100], p_const[100], pz[100], p_vrem[100][100], otvet[100], otvetDv[100], koef[100], y[100], x[100];

int Input() { //Вариант #13, Ответ: 24
    p[1] = p_const[1] = 9;
    p[2] = p_const[2] = 2;
    a[1][1] = a_const[1][1] = a_const1[1][1] = 1;
    a[1][2] = a_const[1][2] = a_const1[1][2] = 2;
    znak[1] = 1;
    b[1] = b_const[1] = 14;
    a[2][1] = a_const[2][1] = a_const1[2][1] = 4;
    a[2][2] = a_const[2][2] = a_const1[2][2] = 11;
    znak[2] = 1;
    b[2] = b_const[2] = 68;
    a[3][1] = a_const[3][1] = a_const1[3][1] = 3;
    a[3][2] = a_const[3][2] = a_const1[3][2] = 1;
    znak[3] = 1;
    b[3] = b_const[3] = 12;
}

int Matrix();
int Matrix_doublex();
int Kanonich();
fractions Output();
fractions Output_doublex();
int Otvet();
int JordanGauss(int ki, int kj);
int find_max();//Симплексное отношение
int find_baz_per();//базовые переменные
int perechet();//пересчет зет строки
int z_gauss(int ki, int kj);//формула прямоугольника
int SimplexTable();
int Simplex();
int Doublex();//к двойственной
int Ravn();//теорема равновессия
int Dvoistven();

int main() {
    cout << "Курсовая работа. Вариант 13 Выполнил: Плющев Филипп" << endl << endl;
    cout << "1. Перейти к канонической форме записи задачи линейного программирования." << endl;
    Input();
    Output();
    cout << endl;
    cout << "Решение:" << endl;
    Kanonich();
    Output();
    cout << endl;

    cout << "2. Написать программу, решающую задачу линейного программированияв канонической форме симплекс-методом, используя в качестве начальной угловой точки ";
    cout << "опорное решение с базисными пересенными X1, X2, X5, найденное методом Жордана-Гусса." << endl << endl;
    cout << "Исходная система в каноническом виде:" << endl;
    Output();
    Simplex();
    cout << endl;

    cout << "4. Составить двойственную задачу к исходной и найти ее решение на основании теоремы равновесия." << endl << endl;
    Dvoistven();

    return 0;
}

int Matrix() {
    cout << endl;
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= m; j++) {
            cout << setw(5);
            a[i][j].print();
            cout << setw(5);
        }
        cout << "  |  ";
        b[i].print();
        cout << endl;
    }
    cout << endl;
}

int Matrix_doublex() {
    cout << endl;
    for (i = 1; i <= m + 1; i++) {
        for (j = 1; j <= n; j++) {
            cout << setw(5);
            a_const[i][j].print();
            cout << setw(5);
        }
        cout << "  |  ";
        if (i == m + 1) cout << "Z'";
        else a_const[i][n + 1].print();
        cout << endl;
    }
    cout << endl;
}

int Kanonich() {
    int vrem = 1;
    for (i = 1; i <= n; i++) {
        if(znak[i] == 1) {
            a[i][m + vrem] = -1;
            znak[i] = 3;
        } else if(znak[i] == 2) {
            a[i][m + vrem] = 1;
            znak[i] = 3;
        }
        vrem++;
    }
    for (i = 1; i <= n; i++)
        for (j = m + 1; j <= m + vrem; j++)
            if ((a[i][j] == 1) || (a[i][j] == -1)) continue;
            else a[i][j] = 0;

    m += vrem - 1;

    for (i = 1; i <= n; i++)
        for (j = 1; j <= m; j++) a_const[i][j] = a[i][j];
}

fractions Output() {
    cout << endl;
    cout << "Целевая функция:" << endl;
    cout << "Z(X1,X2) = ";
    if (flag_out == 1) {
        fractions p_const1 = min_one * p_const[1];
        p_const1.print();
        cout << "*X1 ";
        fractions p_const2 = min_one * p_const[2];
        p_const2.print();
        cout << "*X2 --> max" << endl;
        flag_out++;
    } else {
        p_const[1].print();
        cout << "*X1 + ";
        p_const[2].print();
        cout << "*X2 --> min" << endl;
        flag_out++;
    }

    cout << endl;
    for (i = 1; i <= n; i++) {
        cout << setw(5);
        a_const[i][1].print();
        cout << "*X1";
        for (j = 2; j <= m; j++) {
            if (a_const[i][j] > 0) {
                cout << " + ";
                a_const[i][j].print();
                cout << "*X" << j;
            } else if (a_const[i][j] < 0) {
                per = 0;
                per = per - a_const[i][j];
                cout << " - ";
                per.print();
                cout << "*X" << j;
            }
        }
        if (znak[i] == 1) cout << " >= ";
        else if (znak[i] == 2) cout << " <= ";
        else if (znak[i] == 3) cout << " = ";

        b_const[i].print();
        cout << endl;
    }

    cout << setw(5);
    for (j = 1; j <= m - 1; j++)
        cout << "X" << j << ">=0, ";
    cout << "X" << m << ">=0" << endl;
}

fractions Output_doublex() {
    cout << endl;
    cout << "Целевая функция двойственности:" << endl;
    cout << "Z'(Y1,Y2,Y3) = ";
    a_const[m + 1][1].print();
    cout << "*Y1 + ";
    a_const[m + 1][2].print();
    cout << "*Y2 + ";
    a_const[m + 1][3].print();
    cout << "*Y3 --> max" << endl;

    cout << endl;
    for (i = 1; i <= m; i++) {
        cout << setw(5);
        a_const[i][1].print();
        cout << "*Y1";
        for (j = 2; j <= n; j++) {
            if (a_const[i][j] > 0) {
                cout << " + ";
                a_const[i][j].print();
                cout << "*Y" << j;
            } else if (a_const[i][j] < 0) {
                per = 0;
                per = per - a_const[i][j];
                cout << " - ";
                per.print();
                cout << "*Y" << j;
            }
        }
        if (znak[i] == 1) cout << " >= ";
        else if (znak[i] == 2) cout << " <= ";
        else if (znak[i] == 3) cout << " = ";
        p_const[i].print();
        cout << endl;
    }

    cout << setw(5);
    for (j = 1; j <= m; j++)
        cout << "Y" << j << ">=0, ";
    cout << "Y" << m + 1 << ">=0" << endl;
}

int Otvet() {
    otv = 0;
    cout << " X' = (";
    for (i = 1; i <= m - 1; i++) {
        otvet[i].print();
        cout << "; ";
    }
    otvet[m].print();
    cout << ")" << endl;

    cout << " Z' = ";
    for (i = 1; i <= m - 1; i++) {
        otv = otv + p_const[i] * otvet[i];
        p_const[i].print();
        cout << "*";
        otvet[i].print();
        cout << " + ";
    }
    p_const[m].print();
    cout << "*";
    otvet[m].print();
    cout << " = ";
    otv.print();
    cout << endl;
}

int JordanGauss(int ki, int kj) {
    if ((ki == 0) && (kj == 0)) {
        int k = 1;
        for (ki = kj = 1; ki <= m, kj < m; ki++, kj++) {
            if (a[ki][kj] == 0) continue;
            else {
                if (kj == 3) kj = 5;
                for (i = 1; i <= n; i++)
                    if ((i != ki) && (j != kj)) {
                        b[i] = b[i] - a[i][kj] * b[ki] / a[ki][kj];
                        for (j = 1; j <= m; j++)
                            if ((i != ki) && (j != kj)) a[i][j] = a[i][j] - a[i][kj] * a[ki][j] / a[ki][kj];
                    }

                b[ki] = b[ki] / a[ki][kj];
                for (j = 1; j <= m; j++)
                    if (j != kj) a[ki][j] = a[ki][j] / a[ki][kj];

                for (i = 1; i <= n; i++)

                    if (i == ki) a[i][kj] = 1;
                    else a[i][kj] = 0;

                cout << "Step " << k << ". После действия над " << kj << " столбцом: " << endl;
                k++;
                Matrix();
            }
        }
    } else {
        for (i = 1; i <= n; i++)
            if (i != ki) {
                b[i] = b[i] - a[i][kj] * b[ki] / a[ki][kj];
                for (j = 1; j <= m; j++) if ((i != ki) && (j != kj)) a[i][j] = a[i][j] - a[i][kj] * a[ki][j] / a[ki][kj];
            }

        for (j = 1; j <= m; j++) p[j] = pz[j] - a[ki][j] * pz[kj] / a[ki][kj];

        b[ki] = b[ki] / a[ki][kj];
        for (j = 1; j <= m; j++) if (j != kj) a[ki][j] = a[ki][j] / a[ki][kj];

        for (i = 1; i <= n; i++)
            if (i == ki) a[i][kj] = 1;
            else a[i][kj] = 0;
        p[kj] = 0;
    }
}

int find_max() {
    i_max = 1;
    for (j = 1; j <= m; j++) if (pz[j] > 0) p_max = j;

    minimum = 100;
    for (i = 1; i <= n; i++) koef[i] = b[i] / a[i][p_max];
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= n; j++) {
            if (koef[i] > 0)
                if ((koef[i] < koef[j]) && (koef[i] < minimum)) {
                    minimum = koef[i];
                    i_max = i;
                }
        }
    }

}

int find_baz_per() {
    k = 1;
    for (i = 1; i <= n; i++)
        for (j = 1; j <= m; j++)
            if (a[i][j] == 1) {
                if (((a[i + 1][j] == 0) && (a[i + 2][j] == 0) && (i + 2 <= n)) || ((a[i + 1][j] == 0) && (a[i - 1][j] == 0) && (i + 1 <= n) && (i - 1 >= 1)) || ((a[i - 1][j] == 0) && (a[i - 2][j] == 0) && (i - 2 >= 1))) {
                    baz[k] = j;
                    k++;
                }
            } else if (a[i][j] == -1) {
                if (((a[i + 1][j] == 0) && (a[i + 2][j] == 0) && (i + 2 <= n)) || ((a[i + 1][j] == 0) && (a[i - 1][j] == 0) && (i + 1 <= n) && (i - 1 >= 1)) || ((a[i - 1][j] == 0) && (a[i - 2][j] == 0) && (i - 2 >= 1))) {
                    baz[k] = j;
                    k++;
                    for (int s = 1; s <= m; s++) {
                        a[i][s] = a[i][s] * (-1);
                        b[i] = b[i] * (-1);
                    }
                }
            }
}

int perechet() {
    for (i = 1; i <= 2; i++) {
        for (j = 1; j <= m; j++) if (j != i) p_vrem[i][j] = p[i] * (p_vrem[i][j] - a[i][j]);

        p_vrem[i][m + 1] = b[i] * p[i];
    }

    for (j = 1; j <= m + 1; j++) p[j] = 0;

    for (i = 1; i <= 2; i++)
        for (j = 1; j <= m + 1; j++)
            p[j] = p[j] + p_vrem[i][j];
}

int z_gauss(int ki, int kj) {
    p[m + 1] = p[m + 1] - b[ki] * pz[kj] / a[ki][kj];
}

int SimplexTable() {
    do {
        for (i = 1; i <= m; i++) otvet[i] = 0;

        if (flag != 0) z_gauss(i_max, p_max);
        JordanGauss(i_max, p_max);
        if (flag == 0) {
            cout << "Матрица приведена к нужному виду." << endl;
            cout << "                        Симплекс метод:" << endl << endl;
        }

        if (flag == 0) perechet();
        find_baz_per();

        for (j = 1; j <= m + 1; j++) {
            pz[j] = 0;
            if (flag == 0) pz[j] = pz[j] - p[j];
            else pz[j] = p[j];
        }
        find_max();

        cout << "                       Симплекс-таблица:" << endl;
        cout << "__________________________________________________________________" << endl;
        cout << "|  Базисные  |  Свободные  |      Неизвестные    |  Коэффициент  |" << endl;
        cout << "| переменные |  переменные | X1  X2  X3   X4  X5 |   отношения   |" << endl;
        cout << "|____________|_____________|_____________________|_______________|" << endl;
        for (i = 1; i <= n; i++) {
            cout << "|     X" << baz[i] << "     |      ";

            cout << setw(2);
            b[i].print();
            cout << setw(2);
            cout << "     |  ";

            for (j = 1; j <= m; j++)
                if (flag == 2) {
                    a[i][j].print();
                    cout << setw(4);
                } else {
                    a[i][j].print();
                    cout << setw(3);
                }
            if (flag == 1) cout << "  |   ";
            else if (flag == 2)
                if (i >= 3) cout << " |   ";
                else cout << "  |   ";
            else cout << "  | ";

            if (flag == 0) cout << setw(9);
            else if (flag == 1) cout << setw(7);
            else {
                if ((koef[i] > 0) || (koef[i] == 0)) cout << setw(7);
                else cout << setw(5);
            }
            koef[i].print();
            if (flag != 1) {
                cout << setw(6);
                cout << "|" << endl;
            } else {
                if (i == 3) cout << "  |" << endl;
                else cout << "     |" << endl;
            }
        }

        cout << "|____________|_____________|_____________________|_______________|" << endl;
        for (i = 1; i <= m; i++)
            otvet[baz[i]] = b[i];
        cout << "|     Z      |     ";

        if (flag == 0) p[m + 1].print();
        else pz[m + 1].print();
        cout << "      |  ";

        for (j = 1; j <= m - 1; j++)
            if (flag == 2) {
                pz[j].print();
                cout << setw(4);
            } else {
                pz[j].print();
                cout << setw(3);
            }
        pz[m].print();
        if (flag == 0) cout << "  ";
        if (flag == 1) cout << "  ";
        if (flag == 2) cout << " ";
        cout << "|               |" << endl;

        cout << "|____________|_____________|_____________________|_______________|" << endl;

        Otvet();
        if (flag != 2) {
            cout << " Решение не является оптимальным, так как имеются положительные элементы в нижней строке." << endl;
            cout << endl << endl;
        }
        flag++;
    }
    while ((pz[1] > 0) || (pz[2] > 0) || (pz[3] > 0) || (pz[4] > 0) || (pz[5] > 0));
    cout << " Решение является оптимальным!" << endl;
}

int Simplex() {
    for (i = 1; i <= m; i++)
        for (j = 1; j <= m + 1; j++) {
            otvet[i] = 0;
            p_vrem[i][j] = 0;
        }

    i_max = p_max = 0;
    cout << endl;
    cout << "Решение:" << endl;
    cout << "Представление СЛАУ в виде матрицы:" << endl;
    Matrix();
    SimplexTable();
    sleep(2);
    cout << endl << endl;
    cout << " Ответ: Zmin = Z(";
    for (i = 1; i <= m - 1; i++) {
        otvet[i].print();
        cout << "; ";
    }
    otvet[m].print();
    cout << ") = ";
    pz[m + 1].print();
    cout << endl;
}

int Doublex() {
    for (i = 1; i <= n; i++)
        a_const[i][3] = b[i];
    for (i = 1; i <= n; i++)
        for (j = 1; j <= i; j++) {
            transp = a_const[i][j];
            a_const[i][j] = a_const[j][i];
            a_const[j][i] = transp;
        }

    for (i = 1; i <= n; i++) a_const[m + 1][i] = b_const[i];
    for (i = 1; i <= m; i++) a_const[i][n + 1] = p_const[i];
}

int Ravn() {
    cout << endl << endl;
    cout << "Теорема равновессия для данной задачи имеет вид: " << endl << endl;
    for (i = 1; i <= n; i++) {
        cout << setw(5) << "Y" << i << " * (";
        b_const[i].print();
        cout << " - (";
        for (j = 1; j <= m; j++) {
            y[i] = y[i] + a_const1[i][j] * otvet[j];
            a_const1[i][j].print();
            if (j != m) cout << "*X" << j << " + ";
            else cout << "*X" << j;
        }
        y[i] = b_const[i] - y[i];
        cout << ")) = 0 " << endl;
    }
    for (j = 1; j <= m; j++) {
        cout << setw(5) << "X" << j << " * ((";
        for (i = 1; i <= n; i++) {
            a_const[j][i].print();
            if (i != n) cout << "*Y" << i << " + ";
            else cout << "*Y" << i;
        }
        cout << ") - ";
        a_const[j][n + 1].print();
        cout << ") = 0 " << endl;
    }

    cout << endl;
    cout << "Преобразование системы: " << endl << endl;
    for (i = 1; i <= n; i++) {
        cout << setw(5) << "Y" << i << " * (";
        y[i].print();
        cout << ") = 0 " << endl;
        if ((y[i] > 0) || (y[i] < 0)) otvetDv[i] = 0;
    }
    for (j = 1; j <= m; j++) {
        cout << setw(5);
        otvet[j].print();
        cout << " * (";
        for (i = 1; i <= n; i++) {
            a_const[j][i].print();
            if (i != n) cout << "*Y" << i << " + ";
            else cout << "*Y" << i;
        }
        cout << " - ";
        a_const[j][n + 1].print();
        cout << ") = 0 " << endl;
    }
    cout << endl;

    for (i = 1; i <= n; i++)
        if ((y[i] > 0) || (y[i] < 0)) {
            cout << setw(5) << "Y" << i << " = ";
            otvetDv[i].print();
            cout << endl;
        }
    for (j = 1; j <= m; j++)
        if ((otvet[j] < 0) || (otvet[j] > 0)) {
            cout << setw(5);
            for (i = 1; i <= n; i++) {
                x[i] = otvet[j] * a_const[j][i];
                x[i].print();
                if (i != n) cout << "*Y" << i << " + ";
                else cout << "*Y" << i << " - ";
            }
            x[n + 1] = a_const[j][n + 1] * otvet[j];
            x[n + 1].print();
            cout << " = 0 " << endl;
        }
    cout << endl;

    for (i = 1; i <= n; i++)
        if ((y[i] > 0) || (y[i] < 0)) {
            cout << setw(5) << "Y" << i << " = ";
            otvetDv[i].print();
            cout << endl;
        }
    for (j = 1; j <= m; j++)
        if ((otvet[j] < 0) || (otvet[j] > 0)) {
            cout << setw(5);
            for (i = 1; i <= n; i++) {
                if (y[i] == 0) {
                    x[i].print();
                    cout << "*Y" << i << " - ";
                    otvetDv[i] = x[n + 1] / x[i];
                }
            }
            x[n + 1].print();
            cout << " = 0 " << endl;
        }
    cout << endl;

    for (i = 1; i <= n; i++) {
        cout << setw(5) << "Y" << i << " = ";
        otvetDv[i].print();
        cout << endl;
    }
    cout << endl << endl;

    for (i = 1; i <= n; i++)
        znach_dv = znach_dv + a_const[m + 1][i] * otvetDv[i];
    cout << "Ответ: Ymax = Y(";
    for (i = 1; i <= n - 1; i++) {
        otvetDv[i].print();
        cout << "; ";
    }
    otvetDv[n].print();
    cout << ") = ";
    znach_dv.print();
    cout << endl;
}

int Dvoistven() {
    n = 3;
    m = 2;
    for (i = 1; i <= n; i++) znak[i] = 1;
    cout << "Исходная задача: " << endl;
    Output();
    cout << endl;

    for (i = 1; i <= n; i++) znak[i] = 2;
    cout << "Решение:" << endl;
    Doublex();
    cout << "Матрица, транспонированная от исходной: " << endl;
    Matrix_doublex();
    Output_doublex();
    Ravn();
}
